# NekoSkill

## Prerequire

- Node.js 8.x

## Development

### Environment variables

```bash
$ cp .env.sample .env
$ vim .env
```

- LINE_CHANNEL_SECRET
- LINE_CHANNEL_ACCESS_TOKEN

### Get started

```bash
$ git clone git@gitlab.com:KinokoTeam/LineSeminar/nekoskill.git
$ cd nekoskill
$ npm install
$ npm run build
$ npm run start
```

## Deployment

```bash
$ npm run release-build
```

## Run ngrok

```bash
$ npm install ngrok -g
$ ngrok http 3000
```
