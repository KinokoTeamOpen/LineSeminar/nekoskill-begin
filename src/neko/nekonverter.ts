import { NekoType } from './neko-type';

export class Nekonverter {
  static convertKindToString(kind: NekoType): string {
    let ret: string;
    switch (kind) {
      case NekoType.Munchkin:
        ret = 'マンチカン';
        break;
      case NekoType.ScottishFold:
        ret = 'スコティッシュフォールド';
        break;
      case NekoType.Ragdoll:
        ret = 'ラグドール';
        break;
      case NekoType.MaineCoon:
        ret = 'メインクーン';
        break;
      case NekoType.RussianBlue:
        ret = 'ロシアンブルー';
        break;
      case NekoType.ExoticShorthair:
        ret = 'エキゾチックショートヘア';
        break;
      case NekoType.AmericanShorthair:
        ret = 'アメリカンショートヘア';
        break;
      case NekoType.NorwegianForestCat:
        ret = 'ノルウェージャンフォレストキャット';
        break;
      case NekoType.Somali:
        ret = 'ソマリ';
        break;
      case NekoType.Bengal:
        ret = 'ベンガル';
        break;
      case NekoType.Other:
        ret = 'その他';
        break;
      default:
        new Error(`Unexpected NekoType: ${kind}`);
    }
    return ret;
  }

  static convertStringToKind(str: string): NekoType {
    let ret: NekoType;
    switch (str) {
      case 'マンチカン':
        ret = NekoType.Munchkin;
        break;
      case 'スコティッシュフォールド':
        ret = NekoType.ScottishFold;
        break;
      case 'ラグドール':
        ret = NekoType.Ragdoll;
        break;
      case 'メインクーン':
        ret = NekoType.MaineCoon;
        break;
      case 'ロシアンブルー':
        ret = NekoType.RussianBlue;
        break;
      case 'エキゾチックショートヘア':
        ret = NekoType.ExoticShorthair;
        break;
      case 'アメリカンショートヘア':
        ret = NekoType.AmericanShorthair;
        break;
      case 'ノルウェージャンフォレストキャット':
        ret = NekoType.NorwegianForestCat;
        break;
      case 'ソマリ':
        ret = NekoType.Somali;
        break;
      case 'ベンガル':
        ret = NekoType.Bengal;
        break;
      case 'その他':
        ret = NekoType.Other;
        break;
      default:
        new Error(`Unexpected kind: ${str}`);
    }
    return ret;
  }
}
