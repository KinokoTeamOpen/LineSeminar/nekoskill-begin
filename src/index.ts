require('dotenv').config();
import { Clova, Client, SpeechBuilder, Middleware, Context } from '@line/clova-cek-sdk-nodejs';
import * as express from 'express';

// Express 初期設定
const app = express();
app.get('/', (req, res) => {
  res.send('Hello NekoSkill!');
});

const launchHandler = async (responseHelper: Context) => {
  console.log('========== LaunchRequest ==========');
  responseHelper.setSpeechList([SpeechBuilder.createSpeechText('ねこスキルが起動しました。')]);
};

const intentHandler = async (responseHelper: Context) => {
  const intent = responseHelper.getIntentName();
  const sessionId = responseHelper.getSessionId();
  const userId = process.env.FIX_USER_ID || responseHelper.getUser().userId;

  switch (intent) {
    case 'HelloIntent': {
      console.log('========== HelloIntent ==========');
      responseHelper.setSpeechList([SpeechBuilder.createSpeechText('こんにちは、ねこスキルです。')]);
      break;
    }
    case 'Clova.YesIntent': {
      console.log('========== Clova.YesIntent ==========');
      responseHelper.setSpeechList([SpeechBuilder.createSpeechText('いえすいえす')]);
      break;
    }
    case 'Clova.NoIntent': {
      console.log('========== Clova.NoIntent ==========');
      responseHelper.setSpeechList([SpeechBuilder.createSpeechText('のんのん')]);
      responseHelper.endSession();
      break;
    }
    default:
      console.log('========== Undefiled ==========');
      responseHelper.setSpeechList([SpeechBuilder.createSpeechText('申し訳ございません、聞き取れませんでした。')]);
      break;
  }
};

const sessionEndedHandler = async responseHelper => {};

const clovaHandler = Client.configureSkill()
  .onLaunchRequest(launchHandler)
  .onIntentRequest(intentHandler)
  .onSessionEndedRequest(sessionEndedHandler)
  .handle();

const clovaMiddleware = Middleware({ applicationId: process.env.APPLICATION_ID });

app.post('/', clovaMiddleware, clovaHandler as any);

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Example app listening on port ${port}!`);
});
